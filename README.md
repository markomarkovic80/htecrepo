Champions League Use Case Project


This project is a solution to a Champions League Use Case recruitment task from HTec.
It is developed as a Spring boot web application. 

Application is layered on:
  - Controllers that take user requests via REST api
  - Services that process business logic
  - Repositories that handle database manipulation

Database which is used is H2 in-memory database

Spring boot version is 2.1.5, Java version is 1.8, all other dependencies can be found in pom.xml file inside the project.

Maven is used for building, so "mvn package" command will build the project, run unit tests and create jar file for the solution.

REST api is documented via postman collection inside the base folder of the project

Unit tests with happy paths are placed inside /src/test folder