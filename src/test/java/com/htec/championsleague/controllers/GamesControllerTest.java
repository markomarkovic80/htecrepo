package com.htec.championsleague.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.htec.championsleague.ChampionsLeagueApplication;
import com.htec.championsleague.domain.Game;
import com.htec.championsleague.services.GamesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChampionsLeagueApplication.class)
@AutoConfigureMockMvc
public class GamesControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private GamesService gamesService;

    @Test
    public void testGetGames() throws Exception {
        Game game1 = createGame();
        Game game2 = new Game();
        game2.setGameId(2);
        game2.setGroupId("A");
        game2.setHomeTeam("Ajax");
        game2.setAwayTeam("Milan");
        game2.setLeagueTitle("Premiership 2018/19");
        game2.setMatchday(1);
        game2.setScore("3:2");

        List<Game> gameList = new ArrayList<>();
        gameList.add(game1);
        gameList.add(game2);

        given(gamesService.getAllGames())
                .willReturn(gameList);
        mvc.perform(get("/games")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].leagueTitle", is("Premiership 2018/19")))
                .andExpect(jsonPath("$[0].homeTeam", is("PSG")))
                .andExpect(jsonPath("$[0].matchday", is(1)))
                .andExpect(jsonPath("$[0].score", is("1:0")))
                .andExpect(jsonPath("$[1].leagueTitle", is("Premiership 2018/19")))
                .andExpect(jsonPath("$[1].homeTeam", is("Ajax")))
                .andExpect(jsonPath("$[1].matchday", is(1)))
                .andExpect(jsonPath("$[1].score", is("3:2")));
        reset(gamesService);
    }

    @Test
    public void testCreateGame() throws Exception {
        List<Game> gameList = new ArrayList<>();
        Game game1 = createGame();

        given(gamesService.createGames(any())).willReturn(gameList);

        mvc.perform(post("/games")
                .content(asJsonString(gameList))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        reset(gamesService);
    }

    @Test
    public void testDeleteGame() throws Exception {
        Game game1 = createGame();

        given(gamesService.deleteGame(1)).willReturn(Optional.of(game1));

        mvc.perform(delete("/games/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateGame() throws Exception {
        Game game1 = createGame();
        List<Game> gameList = new ArrayList<>();
        gameList.add(game1);

        given(gamesService.createGames(any())).willReturn(gameList);
        mvc.perform(post("/games")
                .content(asJsonString(gameList))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$[0].score", is("1:0")));

        game1.setScore("5:5");
        given(gamesService.updateGame(any())).willReturn(Optional.of(gameList));
        mvc.perform(put("/games")
                .content(asJsonString(gameList))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].score", is("5:5")));

        reset(gamesService);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Game createGame() {
        Game game = new Game();
        game.setGameId(1);
        game.setGroupId("A");
        game.setHomeTeam("PSG");
        game.setAwayTeam("Arsenal");
        game.setLeagueTitle("Premiership 2018/19");
        game.setMatchday(1);
        game.setScore("1:0");

        return game;
    }

}
