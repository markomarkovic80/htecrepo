package com.htec.championsleague.controllers;

import com.htec.championsleague.ChampionsLeagueApplication;
import com.htec.championsleague.dao.TableDAO;
import com.htec.championsleague.domain.Game;
import com.htec.championsleague.services.GamesService;
import com.htec.championsleague.services.TableService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChampionsLeagueApplication.class)
@AutoConfigureMockMvc
public class TableControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private TableService tableService;

    @MockBean
    private GamesService gamesService;

    @Test
    public void testGetAllTables() throws Exception {

        TableDAO table = getTableDAO();
        List<TableDAO> tableList = new ArrayList<>();
        tableList.add(table);

        given(tableService.getAllTables()).willReturn(tableList);

        mvc.perform(get("/tables")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].group", is("A")));

    }

    @Test
    public void testGetTableForGroupId() throws Exception {
        Game game = createGame();
        List<Game> gameList = new ArrayList<>();
        gameList.add(game);
        TableDAO table = getTableDAO();
        given(gamesService.getGamesForGroupId(any())).willReturn(Optional.of(gameList));
        given(tableService.getTableFromGroupGames(any())).willReturn(table);

        mvc.perform(get("/tables/A")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.group", is("A")));
    }

    private TableDAO getTableDAO() {
        TableDAO table = new TableDAO();
        table.setLeagueTitle("Premiership 2018/19");
        table.setGroup("A");
        table.setMatchday(1);
        return table;
    }

    private Game createGame() {
        Game game = new Game();
        game.setGameId(1);
        game.setGroupId("A");
        game.setHomeTeam("PSG");
        game.setAwayTeam("Arsenal");
        game.setLeagueTitle("Premiership 2018/19");
        game.setMatchday(1);
        game.setScore("1:0");

        return game;
    }
}
