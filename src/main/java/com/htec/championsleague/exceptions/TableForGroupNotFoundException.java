package com.htec.championsleague.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class TableForGroupNotFoundException extends RuntimeException {

    private String groupId;

    public TableForGroupNotFoundException(String groupId) {
        super(String.format("Group not found for group id: %s", groupId));
        this.groupId = groupId;
    }

    public String getId() {
        return groupId;
    }
}
