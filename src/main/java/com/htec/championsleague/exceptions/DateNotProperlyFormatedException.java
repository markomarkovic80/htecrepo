package com.htec.championsleague.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class DateNotProperlyFormatedException extends RuntimeException {
    public DateNotProperlyFormatedException() {
        super(String.format("Provided date is not in good format"));
    }
}
