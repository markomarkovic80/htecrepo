package com.htec.championsleague.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class GamesAdvice {

    @ResponseBody
    @ExceptionHandler(GameNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String gameNotFoundHandler(GameNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(NoGamesToUpdateException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String noGameToUpdateHandler(NoGamesToUpdateException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(TableForGroupNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String tableForGroupNotFound(TableForGroupNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(TableNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String tableNotFound(TableNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(DateNotProperlyFormatedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String dateNotProperlyFormatted(DateNotProperlyFormatedException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(NoGamesMatchingCriteriaFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String noGamesMatchingCriteriaFound(NoGamesMatchingCriteriaFoundException ex) {
        return ex.getMessage();
    }
}
