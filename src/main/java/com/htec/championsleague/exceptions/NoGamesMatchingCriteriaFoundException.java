package com.htec.championsleague.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoGamesMatchingCriteriaFoundException extends RuntimeException {
    public NoGamesMatchingCriteriaFoundException() {
        super("No games matching given criteria found");
    }
}
