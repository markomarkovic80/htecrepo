package com.htec.championsleague.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoGamesToUpdateException extends RuntimeException {

    public NoGamesToUpdateException() {
        super(String.format("No games where valid for update"));
    }
}
