package com.htec.championsleague.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class GameNotFoundException extends RuntimeException {

    private Integer gameId;

    public GameNotFoundException(Integer gameId) {
        super(String.format("Game not found for game id: %d", gameId));
        this.gameId = gameId;
    }

    public Integer getId() {
        return gameId;
    }
}