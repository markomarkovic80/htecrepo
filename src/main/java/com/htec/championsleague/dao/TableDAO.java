package com.htec.championsleague.dao;

import com.htec.championsleague.domain.Rank;

import java.util.List;

public class TableDAO {

    private String leagueTitle;
    private Integer matchday;
    private String group;
    private List<Rank> standing;

    public TableDAO() {

    }

    public TableDAO(String leagueTitle, Integer matchday, String group, List<Rank> standing) {
        this.leagueTitle = leagueTitle;
        this.matchday = matchday;
        this.group = group;
        this.standing = standing;
    }

    public String getLeagueTitle() {
        return leagueTitle;
    }

    public void setLeagueTitle(String leagueTitle) {
        this.leagueTitle = leagueTitle;
    }

    public Integer getMatchday() {
        return matchday;
    }

    public void setMatchday(Integer matchday) {
        this.matchday = matchday;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public List<Rank> getStanding() {
        return standing;
    }

    public void setStanding(List<Rank> standing) {
        this.standing = standing;
    }
}
