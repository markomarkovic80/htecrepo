package com.htec.championsleague.services;

import com.htec.championsleague.domain.Game;
import com.htec.championsleague.repositories.GamesRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GamesServiceImpl implements GamesService {

    private final GamesRepository gamesRepository;

    public GamesServiceImpl(GamesRepository gamesRepository) {
        this.gamesRepository = gamesRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Game> getAllGames() {
        return (ArrayList<Game>) gamesRepository.findAll();
    }

    @Override
    public Iterable<Game> createGames(List<Game> games) {
        return gamesRepository.saveAll(games);
    }

    @Override
    public Optional<List<Game>> updateGame(List<Game> games) {
        List<Game> updatedGames = new ArrayList<>();
        for (Game game : games) {
            Optional<Game> existingGame = gamesRepository.findById(game.getGameId());
            if (existingGame.isPresent()) {
                existingGame.get().setGame(game);
                gamesRepository.save(game);
                updatedGames.add(game);
            }
        }
        return Optional.of(updatedGames);
    }

    @Override
    public Optional<Game> findGameById(Integer gameId) {
        return gamesRepository.findById(gameId);
    }

    @Override
    public Optional<Game> deleteGame(Integer gameId) {
        Optional<Game> existingGame = gamesRepository.findById(gameId);
        existingGame.ifPresent(game -> gamesRepository.deleteById(gameId));
        return existingGame;
    }

    @Override
    public Optional<List<Game>> getGamesForGroupId(String groupId) {
        return gamesRepository.findGamesByGroupId(groupId);
    }

    @Override
    public Optional<List<String>> getDistinctGroupIds() {
        return gamesRepository.getDistinctGroupIds();
    }
}
