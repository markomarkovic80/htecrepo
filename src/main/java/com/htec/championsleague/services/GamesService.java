package com.htec.championsleague.services;

import com.htec.championsleague.domain.Game;

import java.util.List;
import java.util.Optional;

public interface GamesService {
    List<Game> getAllGames();

    Iterable<Game> createGames(List<Game> games);

    Optional<List<Game>> updateGame(List<Game> game);

    Optional<Game> findGameById(Integer gameId);

    Optional<Game> deleteGame(Integer gameId);

    Optional<List<Game>> getGamesForGroupId(String groupId);

    Optional<List<String>> getDistinctGroupIds();
}
