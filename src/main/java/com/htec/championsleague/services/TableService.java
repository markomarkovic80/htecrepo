package com.htec.championsleague.services;

import com.htec.championsleague.dao.TableDAO;
import com.htec.championsleague.domain.Game;

import java.util.List;

public interface TableService {

    TableDAO getTableFromGroupGames(List<Game> games);

    List<TableDAO> getAllTables();
}
