package com.htec.championsleague.services;

import com.htec.championsleague.dao.TableDAO;
import com.htec.championsleague.domain.Game;
import com.htec.championsleague.domain.Rank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TableServiceImpl implements TableService {

    GamesService gamesService;

    @Autowired
    TableServiceImpl(GamesService gamesService) {
        this.gamesService = gamesService;
    }

    @Override
    public List<TableDAO> getAllTables() {
        List<TableDAO> tables = new ArrayList<>();
        Optional<List<String>> uniqueGroupIds = gamesService.getDistinctGroupIds();
        for (String groupId : uniqueGroupIds.get()) {
            Optional<List<Game>> gamesForGroup = gamesService.getGamesForGroupId(groupId);
            if (gamesForGroup.isPresent()) {
                tables.add(getTableFromGroupGames(gamesForGroup.get()));
            }
        }
        return tables;
    }

    @Override
    public TableDAO getTableFromGroupGames(List<Game> games) {
        TableDAO table = new TableDAO();
        table.setLeagueTitle(games.get(0).getLeagueTitle());
        table.setGroup(games.get(0).getGroup());

        List<Integer> matchdays = games.stream()
                .map(Game::getMatchday)
                .collect(Collectors.toList());
        Integer maxMatchDay = matchdays
                .stream()
                .mapToInt(v -> v)
                .max().orElseThrow(NoSuchElementException::new);
        table.setMatchday(maxMatchDay);

        List<Rank> ranks = new ArrayList<>();
        for (Game game : games) {
            if (ranks.size() > 0) {
                boolean added = false;
                for (Rank rank : ranks) {
                    if (rank.getTeam().equalsIgnoreCase(game.getHomeTeam())) {
                        setRankByGame(game, rank, true);
                        added = true;
                    }
                }
                if (!added) {
                    ranks.add(addGameToRank(game, true));
                    added = false;
                }

                for (Rank rank : ranks) {
                    if (rank.getTeam().equalsIgnoreCase(game.getAwayTeam())) {
                        setRankByGame(game, rank, false);
                        added = true;
                    }
                }
                if (!added) {
                    ranks.add(addGameToRank(game, false));
                }
            } else {
                ranks.add(addGameToRank(game, true));
                ranks.add(addGameToRank(game, false));
            }
        }

        ranks.sort(Comparator.comparing(Rank::getPoints).thenComparing(Rank::getGoals).thenComparing(Rank::getGoalDifference).reversed());
        int i = 1;
        for (Rank rank : ranks) {
            rank.setRank(i);
            i++;
        }
        table.setStanding(ranks);

        return table;
    }

    private Rank addGameToRank(Game game, boolean home) {
        Rank rank = new Rank();
        rank.setTeam(home ? game.getHomeTeam() : game.getAwayTeam());
        rank.setPlayedGames(1);
        Integer goals = Integer.parseInt(game.getScore().split(":")[home ? 0 : 1]);
        Integer goalsAgainst = Integer.parseInt(game.getScore().split(":")[home ? 1 : 0]);
        rank.setGoals(goals);
        rank.setGoalsAgainst(goalsAgainst);
        rank.setGoalDifference(goals - goalsAgainst);
        rank.setWin(goals > goalsAgainst ? 1 : 0);
        rank.setLose(goals < goalsAgainst ? 1 : 0);
        rank.setDraw(goals == goalsAgainst ? 1 : 0);
        rank.setPoints(calculatePoints(goals, goalsAgainst));

        return rank;

    }

    private Integer calculatePoints(Integer goals, Integer goalsAgainst) {
        if (goals > goalsAgainst) return 3;
        if (goals < goalsAgainst) return 0;
        return 1;
    }

    private void setRankByGame(Game game, Rank rank, boolean home) {
        rank.setPlayedGames(rank.getPlayedGames() + 1);
        Integer goals = Integer.parseInt(game.getScore().split(":")[home ? 0 : 1]);
        Integer goalsAgainst = Integer.parseInt(game.getScore().split(":")[home ? 1 : 0]);
        rank.setGoals(rank.getGoals() + goals);
        rank.setGoalsAgainst(rank.getGoalsAgainst() + goalsAgainst);
        rank.setGoalDifference(rank.getGoals() - rank.getGoalsAgainst());
        rank.setWin(rank.getWin() + (goals > goalsAgainst ? 1 : 0));
        rank.setLose(rank.getLose() + (goals < goalsAgainst ? 1 : 0));
        rank.setDraw(rank.getDraw() + (goals == goalsAgainst ? 1 : 0));
        rank.setPoints(rank.getPoints() + calculatePoints(goals, goalsAgainst));
    }

}
