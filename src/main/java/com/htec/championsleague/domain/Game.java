package com.htec.championsleague.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "game")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer gameId;

    private String leagueTitle;

    private Integer matchday;

    @Column(name = "groupId")
    private String group;

    private String homeTeam;

    private String awayTeam;

    private Date kickoffAt;

    private String score;


    public Game() {

    }

    public Game(Integer gameId, String leagueTitle, Integer matchday, String group, String homeTeam, String awayTeam, Date kickoffAt, String score) {
        this.gameId = gameId;
        this.leagueTitle = leagueTitle;
        this.matchday = matchday;
        this.group = group;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.kickoffAt = kickoffAt;
        this.score = score;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public String getLeagueTitle() {
        return leagueTitle;
    }

    public void setLeagueTitle(String leagueTitle) {
        this.leagueTitle = leagueTitle;
    }

    public Integer getMatchday() {
        return matchday;
    }

    public void setMatchday(Integer matchday) {
        this.matchday = matchday;
    }

    public String getGroup() {
        return group;
    }

    public void setGroupId(String group) {
        this.group = group;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Date getKickoffAt() {
        return kickoffAt;
    }

    public void setKickoffAt(Date kickoffAt) {
        this.kickoffAt = kickoffAt;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public void setGame(Game game) {
        gameId = game.getGameId();
        leagueTitle = game.getLeagueTitle();
        matchday = game.getMatchday();
        group = game.getGroup();
        homeTeam = game.getHomeTeam();
        awayTeam = game.getAwayTeam();
        kickoffAt = game.getKickoffAt();
        score = game.getScore();
    }

}
