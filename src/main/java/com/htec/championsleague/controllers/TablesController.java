package com.htec.championsleague.controllers;

import com.htec.championsleague.dao.TableDAO;
import com.htec.championsleague.domain.Game;
import com.htec.championsleague.exceptions.TableForGroupNotFoundException;
import com.htec.championsleague.exceptions.TableNotFoundException;
import com.htec.championsleague.services.GamesService;
import com.htec.championsleague.services.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class TablesController {

    GamesService gamesService;
    TableService tableService;

    @Autowired
    public TablesController(GamesService gamesService, TableService tableService) {
        this.gamesService = gamesService;
        this.tableService = tableService;
    }

    @GetMapping(value = "/tables/{groupId}", produces = {"application/json"})
    ResponseEntity<TableDAO> getTableForGroupId(@PathVariable String groupId) {
        Optional<List<Game>> gamesForGroup = gamesService.getGamesForGroupId(groupId);
        if (gamesForGroup.isPresent()) {
            TableDAO tableForGroup = tableService.getTableFromGroupGames(gamesForGroup.get());
            return new ResponseEntity<>(tableForGroup, HttpStatus.OK);
        }

        throw new TableForGroupNotFoundException(groupId);
    }

    @GetMapping(value = "/tables", produces = {"application/json"})
    ResponseEntity<List<TableDAO>> getTables() {
        List<TableDAO> gamesForGroup = tableService.getAllTables();
        if (gamesForGroup.size() > 0) {
            return new ResponseEntity<>(gamesForGroup, HttpStatus.OK);
        }

        throw new TableNotFoundException();
    }
}
