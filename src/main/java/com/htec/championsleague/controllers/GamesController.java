package com.htec.championsleague.controllers;

import com.htec.championsleague.domain.Game;
import com.htec.championsleague.exceptions.DateNotProperlyFormatedException;
import com.htec.championsleague.exceptions.GameNotFoundException;
import com.htec.championsleague.exceptions.NoGamesMatchingCriteriaFoundException;
import com.htec.championsleague.exceptions.NoGamesToUpdateException;
import com.htec.championsleague.services.GamesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class GamesController {

    private final GamesService gamesService;

    @Autowired
    public GamesController(GamesService gamesService) {
        this.gamesService = gamesService;
    }

    @GetMapping(value = "/games", produces = {"application/json"})
    ResponseEntity<List<Game>> getGames() {
        List<Game> games = gamesService.getAllGames();
        return new ResponseEntity<>(games, HttpStatus.OK);
    }

    @PostMapping(value = "/games", produces = {"application/json"})
    ResponseEntity<List<Game>> createGames(@RequestBody List<Game> games) {
        List<Game> createdGames = (ArrayList<Game>) gamesService.createGames(games);
        return new ResponseEntity<>(createdGames, HttpStatus.CREATED);
    }

    @PutMapping(value = "/games", produces = {"application/json"})
    ResponseEntity<List<Game>> updateGames(@RequestBody List<Game> games) {
        Optional<List<Game>> updatedGames = gamesService.updateGame(games);
        if (updatedGames.isPresent()) {
            return new ResponseEntity<>(updatedGames.get(), HttpStatus.OK);
        }
        throw new NoGamesToUpdateException();
    }

    @GetMapping(value = "/games/{gameId}", produces = {"application/json"})
    Resource<Game> getGameById(@PathVariable Integer gameId) {
        Optional<Game> game = gamesService.findGameById(gameId);
        if (!game.isPresent()) {
            throw new GameNotFoundException(gameId);
        }

        return new Resource<>(game.get(),
                linkTo(methodOn(GamesController.class).getGameById(gameId)).withSelfRel(),
                linkTo(methodOn(GamesController.class).getGames()).withRel("games"));

    }

    @DeleteMapping(value = "/games/{gameId}", produces = {"application/json"})
    void deleteGames(@PathVariable Integer gameId) {
        if (!gamesService.deleteGame(gameId).isPresent()) {
            throw new GameNotFoundException(gameId);
        }
    }

    @GetMapping(value = "/filteredGames", produces = {"application/json"})
    ResponseEntity<List<Game>> getFilteredGame(@RequestParam(value = "groupId", required = false) String groupId,
                                               @RequestParam(value = "team", required = false) String team,
                                               @RequestParam(value = "dateFrom", required = false) String stringDateFrom,
                                               @RequestParam(value = "dateTo", required = false) String stringDateTo) {
        List<Game> games = gamesService.getAllGames();
        List<Game> filteredGames = new ArrayList<>();
        if (games.size() > 0) {
            if (groupId != null) {
                filteredGames.addAll(games.stream()
                        .filter(game -> game.getGroup().equalsIgnoreCase(groupId)).collect(Collectors.toList()));
            }
            if (team != null) {
                filteredGames.addAll(games.stream()
                        .filter(game -> game.getHomeTeam().equalsIgnoreCase(team)).collect(Collectors.toList()));
                filteredGames.addAll(games.stream()
                        .filter(game -> game.getAwayTeam().equalsIgnoreCase(team)).collect(Collectors.toList()));
            }
            if (stringDateFrom != null && stringDateTo != null) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date dateFrom;
                Date dateTo;
                try {
                    dateFrom = formatter.parse(stringDateFrom);
                    dateTo = formatter.parse(stringDateTo);
                } catch (ParseException e) {
                    throw new DateNotProperlyFormatedException();
                }
                filteredGames.addAll(games.stream()
                        .filter(game -> game.getKickoffAt().after(dateFrom) && game.getKickoffAt().before(dateTo)).collect(Collectors.toList()));
            }
            return new ResponseEntity<>(filteredGames, HttpStatus.OK);
        }
        throw new NoGamesMatchingCriteriaFoundException();
    }

}
