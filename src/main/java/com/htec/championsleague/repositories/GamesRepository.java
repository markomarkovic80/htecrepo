package com.htec.championsleague.repositories;

import com.htec.championsleague.domain.Game;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface GamesRepository extends CrudRepository<Game, Integer> {

    @Query("SELECT g FROM Game g WHERE g.group = :groupId")
    Optional<List<Game>> findGamesByGroupId(@Param("groupId") String groupId);

    @Query("SELECT DISTINCT g.group FROM Game g")
    Optional<List<String>> getDistinctGroupIds();
}
